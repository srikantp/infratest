FROM amd64/ubuntu:18.04

RUN apt-get update && apt-get install -y curl dbus kmod iproute2 iputils-ping net-tools openssh-server \
    rng-tools gnupg2 sudo systemd udev vim wget gnupg2 && \
    apt-get clean && rm -rf /var/lib/apt/lists/*

RUN curl -s https://download.docker.com/linux/ubuntu/gpg | apt-key add -
RUN curl -s https://packages.cloud.google.com/apt/doc/apt-key.gpg | apt-key add -
RUN echo deb [arch=amd64] https://download.docker.com/linux/ubuntu bionic stable > /etc/apt/sources.list.d/docker.list
RUN echo deb http://apt.kubernetes.io/ kubernetes-xenial main > /etc/apt/sources.list.d/kubernetes.list

RUN export DEBIAN_FRONTEND=noninteractive && apt-get update && apt-get install -y --no-install-recommends \
    htop git docker-ce docker-compose awscli nano sudo acl dnsutils telnet bash-completion && \
	apt-get clean && rm -rf /var/lib/apt/lists/*

RUN export DEBIAN_FRONTEND=noninteractive && apt-get update && apt-get install -y --no-install-recommends \
	less nfs-kernel-server nfs-common locales netcat pass auditd audispd-plugins unzip xz-utils jq file \
	kubeadm=1.18.4-00 kubectl=1.18.4-00 kubelet=1.18.4-00 lsb-release rsyslog && apt-mark hold kubeadm kubectl kubelet && \
	apt-get clean && rm -rf /var/lib/apt/lists/*

RUN wget https://github.com/mikefarah/yq/releases/download/3.3.2/yq_linux_amd64 -O /usr/local/bin/yq && \
    chmod a+x /usr/local/bin/yq

RUN ln -fs /usr/share/zoneinfo/Asia/Kolkata /etc/localtime
RUN echo "" > /etc/machine-id && echo "" > /var/lib/dbus/machine-id
RUN sed -i -e 's/^AcceptEnv LANG LC_\*$/#AcceptEnv LANG LC_*/' /etc/ssh/sshd_config
RUN echo "root:root" | chpasswd
RUN useradd -m -u 9999 -s /bin/bash maint && echo "maint:admin" | chpasswd 
RUN adduser maint sudo && usermod -aG docker maint && echo '%sudo ALL=(ALL) NOPASSWD:ALL' >> /etc/sudoers
RUN echo "net.ipv4.ip_forward=1" > /etc/sysctl.conf
COPY ./isopub.pem ./init.sh /home/maint/
RUN mkdir -p /home/maint/.ssh && cat /home/maint/isopub.pem > /home/maint/.ssh/authorized_keys && \
	chmod 700 -R /home/maint/.ssh && chmod 600 /home/maint/.ssh/authorized_keys && \
	chown -R maint.maint /home/maint/.ssh && rm /home/maint/isopub.pem
