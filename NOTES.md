
NOTES Infrasetup
================

#. All tests will be performed on single VM Isla setups
#. The IPs, certs and other variables should be hard coded on these test images
#. Developer must try on local single VM setup and set the conffile `cluster.csv` or `cluster.yaml` so as to scp to test machine
#. image: rc2-raw, rc1-raw, poc-raw (prebuilt qemu images with static IPs made from respective ISOs)
#. image: rc2-presetup (prebuilt with IP, islasetup and kubernetes running)
#. test: ls -l /home/maint/