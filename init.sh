#!/bin/bash

sudo hostnamectl set-hostname isla
cat << EOT >/tmp/hosts
127.0.0.1	localhost
127.0.0.1	isla
# The following lines are desirable for IPv6 capable hosts
::1     ip6-localhost ip6-loopback
fe00::0 ip6-localnet
ff00::0 ip6-mcastprefix
ff02::1 ip6-allnodes
ff02::2 ip6-allrouters
EOT
sudo cp /tmp/hosts /etc/hosts
